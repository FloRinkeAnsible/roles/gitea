---
# file: roles/fr.gitea/tasks/main.yml

- name: Set OS dependent variables
  include_vars: "{{ item }}"
  with_first_found:
   - "{{ ansible_distribution }}_{{ ansible_distribution_major_version }}.yml"
   - "{{ ansible_distribution }}.yml"
   - "{{ ansible_os_family }}_{{ ansible_distribution_major_version }}.yml"
   - "{{ ansible_os_family }}.yml"
   - default.yml

- name: OS is supported
  assert:
    that: gitea__supported|bool

- name: Ensure sha256sum is available
  tags: gitea
  assert:
    that: gitea__sha256sum[gitea__version] is defined

- name: Create gitea user
  tags: gitea
  user:
    name: "{{ gitea__username }}"
    home: "{{ gitea__home }}"
    system: true

- name: Install prerequisites
  tags: gitea
  package:
    name: git
    state: present

- name: Create directorys
  tags: gitea
  with_items:
    - "{{ gitea__home ~ '/dl' }}"
    - "{{ gitea__home ~ '/custom' }}"
    - "{{ gitea__home ~ '/custom/data' }}"
    - "{{ gitea__home ~ '/.ssh' }}"
    - "{{ gitea__install_dir }}"
    - "{{ gitea__logdir }}"
    - "{{ gitea__config_dir }}"
  file:
    state: directory
    path: "{{ item.path|default(item) }}"
    owner: "{{ item.owner|default(gitea__username) }}"
    group: "{{ item.group|default(gitea__username) }}"
    mode: 0755

- name: Download gitea
  get_url:
    url: "https://dl.gitea.io/gitea/{{ gitea__version }}/{{ gitea__filename }}"
    dest: "{{ gitea__home }}/dl/{{ gitea__filename }}"
    checksum: "sha256:{{ gitea__checksum }}"
    dest: "{{ gitea__home }}/dl/{{ gitea__filename }}"
    owner: "{{ gitea__username }}"
    group: "{{ gitea__username }}"

- name: Install current version
  copy:
    remote_src: true
    src: "{{ gitea__home }}/dl/{{ gitea__filename }}"
    dest: "{{ gitea__install_dir }}/gitea"
    owner: "{{ gitea__username }}"
    group: "{{ gitea__username }}"
    mode: 0754

- name: Link config
  file:
    state: link
    src: "{{ gitea__config_dir }}"
    dest: "{{ gitea__home }}/custom/conf"

- name: Deploy config
  notify: restart gitea
  template:
    src: "{{ gitea__appini_template }}"
    dest: "{{ gitea__config_dir }}/app.ini"
    owner: "{{ gitea__username }}"
    group: "{{ gitea__username }}"
    mode: 0640

- name: Deploy unitfiles
  template:
    src: gitea.service.j2
    dest: /etc/systemd/system/gitea.service

- name: Ensure gitea service is enabled and started
  service:
    state: started
    enabled: true
    name: gitea
    daemon_reload: true

...
